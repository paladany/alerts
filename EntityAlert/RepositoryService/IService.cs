﻿using AlertEntityClassLibrary;
using System.Collections.Generic;


namespace RepositoryService
{
	public interface IService
	{
		IEnumerable<IAlertFilter> GetAlertFilters(IEntity entity);
	}
}
