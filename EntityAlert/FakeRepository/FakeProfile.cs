﻿using AlertEntityClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeRepository
{
	public class FakeProfile : IEntity
	{
		public EntityType AlertEntityType
		{
			get
			{
				return EntityType.Profile;
			}
		}

		[AlertField("Dob")]
		public DateTime Dob { get; set; }

		[AlertField("RoomId")]
		public string PreferedRoomId { get; set; }

		[AlertField("RoomType")]
		public string PreferedRoomType { get; set; }
	}
}
