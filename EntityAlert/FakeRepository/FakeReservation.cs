﻿using AlertEntityClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeRepository
{
	public class FakeReservation : IEntity
	{
		public EntityType AlertEntityType
		{
			get
			{
				return EntityType.Reservation;
			}
		}

		[AlertField("Arrival")]
		public DateTime From { get; set; }

		[AlertField("Departure")]
		public DateTime To { get; set; }
	}
}
