﻿using AlertEntityClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeRepository
{
	public class FakeAlertFilter : IAlertFilter
	{
		public EntityType ComparisonEntityType { get; set; }

		public string ComparisonField { get; set; }

		public ComparisonType ComparisonType { get; set; }

		public string ComparisonValue { get; set; }

	}
}
