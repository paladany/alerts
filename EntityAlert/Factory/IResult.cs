﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
	public interface IResult
	{
		string FilterName { get; set; }
		object PropertyValue { get; set; }
	}
}
