﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
	public class Result : IResult
	{
		public Result(string filterName, object PropertyValue)
		{
			this.FilterName = filterName;
			this.PropertyValue = PropertyValue;
		}
		public string FilterName { get; set; }

		public object PropertyValue { get; set; }

	}
}
