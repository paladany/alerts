﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
	public class EntityProperty
	{
		public EntityProperty(string name, object value)
		{
			this.Name = name;
			this.Value = value;
		}
		public string Name { get; set; }
		public object Value { get; set; }
	}
}
