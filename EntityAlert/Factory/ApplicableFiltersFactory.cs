﻿using AlertEntityClassLibrary;
using RepositoryService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Factory
{
	public class ApplicableFiltersFactory
	{
		public IEnumerable<IResult> CheckEnityForApplicableFilters(IEntity entity, IService service)
		{
			var results = new List<IResult>();
			var filters = GetAlertFilters(entity, service);
			var properties = GetEntityProperties(entity).ToList();

			foreach (var property in properties)
			{
				var propFilters = filters.Where(f => f.ComparisonField == property.Name);

				if (propFilters.Any())
				{
					//now check what filters apply 
					foreach (var filter in propFilters)
					{
						if (IsFilterApplicable(filter, property))
						{
							results.Add(new Result(property.Name, property.Value));
						}
					}
				}
			}

			return results;
		}

		private bool IsFilterApplicable(IAlertFilter filter, EntityProperty property)
		{
			//todo: this part is only for proof of concept

			var isApplicable = false;
			switch (filter.ComparisonType)
			{
				case ComparisonType.Equal:

					isApplicable = filter.ComparisonValue == Convert.ToString(property.Value);
					break;

				case ComparisonType.NotEqual:

					isApplicable = filter.ComparisonValue != Convert.ToString(property.Value);
					break;

				case ComparisonType.Less:
					isApplicable = Convert.ToDecimal(property.Value) < Convert.ToDecimal(filter.ComparisonValue);

					break;
				case ComparisonType.Greather:

					isApplicable = Convert.ToDecimal(filter.ComparisonValue) < Convert.ToDecimal(property.Value);
					break;

				case ComparisonType.Between:
					var btSeparator = new string[] { "~" };
					var btValues = (filter.ComparisonValue).Split(btSeparator, StringSplitOptions.None).ToList();
					var lower = btValues[0];
					var higher = btValues[1];

					isApplicable = Convert.ToDecimal(lower) < Convert.ToDecimal(property.Value) && Convert.ToDecimal(property.Value) < Convert.ToDecimal(higher);

					break;

				case ComparisonType.Contains:

					isApplicable = ((string)property.Value).Contains(filter.ComparisonValue);
					break;

				case ComparisonType.In:

					var separator = new string[] { "~" };
					var filterValues = (filter.ComparisonValue).Split(separator, StringSplitOptions.None).ToList();

					isApplicable = filterValues.Contains((string)property.Value);
					break;
				default:

					isApplicable = false;
					break;
			}

			return isApplicable;
		}

		private static IEnumerable<EntityProperty> GetEntityProperties(IEntity entity)
		{

			//TODO:we can get only the properties that are present in the filters
			var props = entity.GetType()
								.GetProperties()
								 .Where(prop => Attribute.IsDefined(prop, typeof(AlertFieldAttribute)))
								 .Select(prop => new EntityProperty(((AlertFieldAttribute)(prop.GetCustomAttribute(typeof(AlertFieldAttribute)))).EntityField, prop.GetValue(entity)));

			return props;
		}

		private IEnumerable<IAlertFilter> GetAlertFilters(IEntity entity, IService service)
		{
			var filters = service.GetAlertFilters(entity);

			return filters;
		}


	}


}
