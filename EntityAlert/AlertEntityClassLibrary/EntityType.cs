﻿namespace AlertEntityClassLibrary
{
	public enum EntityType
	{
		Profile,
		Reservation,
		Product
	}
}