﻿
namespace AlertEntityClassLibrary
{
	public interface IAlertFilter
	{

		EntityType ComparisonEntityType { get; set; }

		string ComparisonField { get; set; }

		ComparisonType ComparisonType { get; set; }

		//This may change .. at this point we will keep everithing in a string
		string ComparisonValue { get; set; }


	}
}
