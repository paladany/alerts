﻿namespace AlertEntityClassLibrary
{
	public enum ComparisonType
	{
		Equal,
		NotEqual,
		Less,
		Greather,
		Between,
		Contains,
		In
	}
}