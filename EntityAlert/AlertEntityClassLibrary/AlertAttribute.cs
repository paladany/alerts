﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertEntityClassLibrary
{
	[AttributeUsage(AttributeTargets.Property)]
	public class AlertFieldAttribute : Attribute
	{
		private string entityField;

		public AlertFieldAttribute(string fieldName)
		{
			this.entityField = fieldName;
		}

		public virtual string EntityField
		{
			get { return entityField; }
		}
	}
}
