﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlertEntityClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Factory;
using Tests.FakeObjects;

namespace AlertEntityClassLibrary.Tests
{
	[TestClass()]
	public class FactoryTests
	{


		[TestMethod()]
		public void FilterProfileTest()
		{
			//Arange

			var profile = new FakeProfile()
			{
				Dob = DateTime.Now.AddDays(10),
				PreferedRoomId = "100",
				PreferedRoomType = "Double",
				VipLevel = 1,
				BlackListed = false
			};

			var service = new FakeService();
			var factory = new ApplicableFiltersFactory();

			//Act

			var result = factory.CheckEnityForApplicableFilters(profile, service);

			//Assert 

			Assert.AreEqual(3, result.Count());

		}

		[TestMethod()]
		public void FilterProfileWithVipLevelTest()
		{
			//Arange

			var profile = new FakeProfile()
			{
				Dob = DateTime.Now.AddDays(32),
				PreferedRoomId = "100",
				PreferedRoomType = "Double",
				VipLevel = 3,
				BlackListed = false
			};

			var service = new FakeService();
			var factory = new ApplicableFiltersFactory();

			//Act

			var result = factory.CheckEnityForApplicableFilters(profile, service);

			//Assert 

			Assert.AreEqual(3, result.Count());

		}
	}
}