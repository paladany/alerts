﻿using AlertEntityClassLibrary;
using RepositoryService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.FakeObjects
{
	public class FakeAlertFilter : IAlertFilter
	{

		public FakeAlertFilter(EntityType type, string field, ComparisonType comparisonType, string value)
		{
			this.ComparisonEntityType = type;
			this.ComparisonField = field;
			this.ComparisonType = comparisonType;
			this.ComparisonValue = value;
		}
		public EntityType ComparisonEntityType { get; set; }

		public string ComparisonField { get; set; }

		public ComparisonType ComparisonType { get; set; }

		public string ComparisonValue { get; set; }

	}
}
