﻿using RepositoryService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlertEntityClassLibrary;

namespace Tests.FakeObjects
{
	class FakeService : IService
	{
		public IEnumerable<IAlertFilter> GetAlertFilters(IEntity entity)
		{
			var filters = new List<FakeAlertFilter>();

			filters.Add(new FakeAlertFilter(type: EntityType.Profile,
										field: "DOB",
										comparisonType: ComparisonType.Less,
										value: "30"
										));

			filters.Add(new FakeAlertFilter(type: EntityType.Profile,
										field: "RoomId",
										comparisonType: ComparisonType.NotEqual,
										value: ""
										));

			filters.Add(new FakeAlertFilter(type: EntityType.Profile,
											field: "RoomType",
											comparisonType: ComparisonType.NotEqual,
											value: ""
											));
			filters.Add(new FakeAlertFilter(type: EntityType.Profile,
											field: "VIPLevel",
											comparisonType: ComparisonType.Between,
											value: "2~5"
											));
			filters.Add(new FakeAlertFilter(type: EntityType.Profile,
											field: "Blacklisted",
											comparisonType: ComparisonType.Equal,
											value: "true"
											));
			return filters;
		}


	}
}
