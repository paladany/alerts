﻿using AlertEntityClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.FakeObjects
{
	public class FakeProfile : IEntity
	{
		public EntityType AlertEntityType
		{
			get
			{
				return EntityType.Profile;
			}
		}


		public DateTime Dob { get; set; }

		[AlertField("DOB")]
		public int DaysUntillDob
		{
			get
			{
				var today = DateTime.Now;
				var NextDob = new DateTime(today.Year, Dob.Month, Dob.Day);
				return (NextDob - today.Date).Days;
			}
		}

		[AlertField("RoomId")]
		public string PreferedRoomId { get; set; }

		[AlertField("RoomType")]
		public string PreferedRoomType { get; set; }

		[AlertField("VIPLevel")]
		public int VipLevel { get; set; }

		[AlertField("Blacklisted")]
		public bool BlackListed { get; set; }
	}
}
